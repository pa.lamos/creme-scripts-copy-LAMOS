# -*- coding: utf-8 -*-
"""
@author: tgateau
"""

from optparse import OptionParser
import configparser
import math
import numpy as np
import json
import os
import csv
#from pyquaternion import Quaternion
from collections import OrderedDict 
import sys
import os
import yaml

import yamlloader
import copy

#input("Press Enter to continue...")

#elevation in degree
def getSlantRange(h,elevation=5,R_e=6378.136):
    r = h + R_e
    elevation = math.radians(elevation)
    return R_e*(math.sqrt((r**2/R_e**2-math.cos(elevation)**2))-math.sin(elevation)) 
   
#Slant Range in m 
def getFreeSpaceLoss(r,waveLength):
    """get free space losses according to radius, following a spherical model """
    return (4*math.pi*r/waveLength)**2

def getFreeSpaceLoss_dB(r,waveLength):
    return 22.0 + 20*math.log (r/waveLength,10)

def to_db(v):
    """convert to decibels """
    return math.log(float(v), 10) * 10

def from_db(v):
    """convert from dB """
    return 10**(float(v)/10.0)   
    
def compute_elevation_angle(Emin,Pos_EME_Sat,Pos_EME_Sta):
    """compute elevation angle over time given tables of satellite and station positions over time"""
    V_SS_2000 = [0]*len(Pos_EME_Sat)
    for i in range(len(Pos_EME_Sat)):
        V_SS_2000[i] = Pos_EME_Sta[i] - Pos_EME_Sat[i]
    Mag_Vss_2000 = np.linalg.norm(V_SS_2000)
    Mag_Pos_EME_Sta = np.linalg.norm(Pos_EME_Sta)
    Theta = np.degrees(math.acos(np.dot(np.asarray(Pos_EME_Sta),-np.asarray(V_SS_2000))/(Mag_Pos_EME_Sta*Mag_Vss_2000)))
    if Theta<=(90-Emin): Elevation_angle = 90-Theta
    else: Elevation_angle = 0
    return Elevation_angle 

def compute_doppler_shift(Pos_EME_Sat,Pos_EME_Sta,time,f,c):
    """compute doppler shift over time given tables of satellite, stations position and timescale, and transmitter frequency"""
    V_SS_2000 = [0]*len(Pos_EME_Sat)
    i = 0
    while i<len(Pos_EME_Sat):
        V_SS_2000[i] = Pos_EME_Sta[i] - Pos_EME_Sat[i]
        i+=1
    Mag_Vss_2000 = np.linalg.norm(V_SS_2000)
    position_dt = np.diff(Mag_Vss_2000)/np.diff(np.asarray(time))
    doppler_shift = -position_dt*f/c
    return doppler_shift
    

def compute_distance(pt_1, pt_2):
    pt_1 = np.array((pt_1[0], pt_1[1]))
    pt_2 = np.array((pt_2[0], pt_2[1]))
    return np.linalg.norm(pt_1-pt_2)

def closest_node(node, nodes):
    pt = []
    dist = 9999999
    for n in nodes:
        if compute_distance(node, n) <= dist:
            dist = compute_distance(node, n)
            pt = n
    return pt

def get_gain (el, az, el_az,gain):
    closest = closest_node([el,az], el_az)
    index = el_az.index(closest)
    gain = gain[index]
    return gain

def computeParabolicGainFromDiameter(efficiency,diametre,frequency,lightSpeed = 299792458):
    """diameter (m)  frequency (Hz)
    return gain in dB
    """
    eta_r = efficiency
    D=diametre
    c= lightSpeed   
    f = frequency
    waveLength = c/f              
    gain_r = eta_r * (math.pi * D / waveLength)**2
    gain_r_dB = to_db(gain_r)
    return gain_r_dB
    
#computeParabolicGainFromDiameter(0.65,3,2.290e9)

def prettyPrint(myStr,val,maxSpace=20):
    nSpace = " "*(maxSpace-len(myStr))
    resStr = myStr+nSpace+": "+str(val)
    print(resStr)
    return resStr


def prettyD(d, indent=0):
   for key, value in d.items():
      print('\t' * indent + str(key))
      if isinstance(value, dict):
         prettyD(value, indent+1)
      else:
         print('\t' * (indent+1) + str(value))

def prettyStr(d, indent=0):
    myStr = ''
    for key, value in d.items():
        myStr += '    ' * indent + str(key)
        myStr += "\n"
        if isinstance(value, dict):
            myStr += prettyStr(value, indent+1)
            myStr += "\n"        
        else:
            myStr += '    ' * (indent+1) + str(value)
            myStr += "\n"  
    return  myStr


#|ORBIT||||
#||SMA|6978.14|km|


def prettyMD(d, indent=4, sep = '|'):
    myStr = ''
    for key, value in d.items():
        
        if indent<3:
            myStr += sep 
            myStr += sep + str(key) + sep   
        else:
            myStr += sep + str(key) + indent*sep 
        
        if indent>2:
            myStr += "\n"
            
        if isinstance(value, dict):     
            myStr +=  prettyMD(value, indent-1)   
            
        else:
            myStr += str(value[0])+sep+str(value[1])+sep
            myStr += "\n"             
    return  myStr

def fdelUnits(config_dict):
    new_config_without_unit = copy.deepcopy(config_dict)
    for key,value in config_dict.items():
        for key_1,value_1 in value.items():
            if type(value_1) is str:
                if '[' in value_1 :
                    new_value = value_1[:-1].split('[')
                    new_config_without_unit[key][key_1] = float(new_value[0])
                else:
                    new_config_without_unit[key][key_1] = value_1
            else:
                new_config_without_unit[key][key_1] = value_1
    return new_config_without_unit

def writeprettyD(d, init_string, indent=0):
    
    for key, value in d.items():
      res_string += "\t" * indent + str(key)

      res_string += "\t" * (indent+1) + str(value)
    return res_string
         
def prettyIni(d, indent=0):
   for key, value in d.items(): 
      print('\t' * indent + str(key))  
      for k, v in value.items(): 
          print('\t' * (indent+1) +str(k)+":"+str(v))

class LinkBudgetAnalysis:
    def __init__(self,yamlData,verbose=False):
        """Provide data for link budget analysis"""
        
        self.verbose =   verbose  
        if self.verbose:  print ("Verbose on")
        self.modeRadioHam = True
        if type(yamlData)==dict: #we suppose it is data formatted in json string
            if self.verbose: print ("Reading Json Data from a json dict")
            self.config = yamlData
        elif type(yamlData)==str:            
            if yamlData[-5:]==".json": #we suppose  data formatted in json string, in a json ascii file
                if self.verbose: print ("Reading Json Data from a json file")
                self.config = json.load(open(yamlData))                
            else: #configFilename[-4:]==".ini": #we suppose  it's a .ini file format
                if self.verbose: print ("Reading .ini Data from a .ini file")
                self.config = configparser.ConfigParser()
                self.config.read(yamlData)                 
        else:
            print("ERROR")
            print("=================")            
            print(yamlData)
            
        self.altitude = float(self.config['SPACECRAFT_FLIGHT_DYNAMICS']['SC_altitude'])

        
        self.initSynthesis()
        
        
    def initSynthesis(self):
        
        self.synthesis = OrderedDict()
        self.synthesis["Downlink"] = OrderedDict()
        self.synthesis["Uplink"] = OrderedDict()
        
        for param in ["Tx","Path","Rx","Synthesis"]:
            self.synthesis["Downlink"][param] =  OrderedDict()
            self.synthesis["Uplink"][param] =  OrderedDict()   
        


    def addToSynthesis(self,myStr,val,unit="dB"):
        self.maxSpace = 20
        #nSpace = " "*(self.maxSpace-len(myStr))
        #resStr = myStr+ " ["+unit+"]"+nSpace+": "
        if self.verbose:prettyPrint(myStr,str(val))
        #print(part)
        #print(subpart)
        #print(myStrIndent)        
        self.synthesis[self.synthPart][self.synthSubPart][myStr]=(val,unit)
        
            
    def loadOEM_AEM(self):
        print("Laoding OEM file...")
        self.load_satellite_OEM_data()
        print("Done\nLaoding AEM file...")
        self.load_satellite_AEM_data()   
        print("Done\nLaoding station_OEM file...")        
        self.load_station_OEM_data()
        print("Done")        

    def updateJson(self,yamlData):
        self.config.update(yamlData)
        
    
    #awfull way to update it, but mheeee, it's done !    
    def updateJsonUniqueKey(self,distantData):        
        for distantKey in  list(distantData.keys()):
            for k in list(self.config.keys()):
                #print("MENU "+ k)
                for kk in list(self.config[k].keys()):
                    #print("checking "+ kk)
                    if kk==distantKey:
                        if self.verbose:print(distantKey + " FOUND")
                        if not distantData[distantKey]==None:
                            self.config[k][kk] = distantData[distantKey]
                            if self.verbose:print(distantKey + " Updated")
                        

    def get_satellite_OEM_data(self):
            return self.oem_data
 
   
    def load_satellite_OEM_data(self):
        """"read satellite OEM file provided and extract the its data in a list"""
        #        nb_satellites = self.config['SPACECRAFT_FLIGHT_DYNAMICS']['nb_satellites']
        #        if nb_satellite==1:
        # Open the file
        file = self.config['SPACECRAFT_FLIGHT_DYNAMICS']['filename_sat_OEM']
        headerlines =  int(self.config['SPACECRAFT_FLIGHT_DYNAMICS']['headerlines_sat_OEM'])
        #filename = os.path.join('data', file)
        filename = file
        f = open(filename, "r")
        # Read the contents
        contents = f.readlines()[headerlines:]
        f.close()
        # Split the data based on the comma delimmiter
        data = []
        for line in contents:
            coordinates = line.split() 
            data = data + coordinates
        self.oem_data = []
        saved_set = []
        tally = 0
        for value in data:
            if not(value == '\s'):
                val = float(value)
                tally += 1
                if tally < 5:
                    # save the data to the set
                    saved_set.append(val)
                elif tally == 5:
                    # Save the data and the set
                    saved_set.append(val)
                    self.oem_data.append(saved_set)
                else:
                    # Reset tally and saved saved set
                    saved_set = []
                    # Save the data and the set
                    saved_set.append(val)
                    tally = 1

        
        # self.oem_data is ready to be used
        
    #        else:
    #            satellites_OEM = []
    #            for i in range(nb_satellites):
    #                file = self.config['SPACECRAFT_FLIGHT_DYNAMICS']['filename_sat_OEM'+str(i+1)]
    #                headerlines = int(self.config['SPACECRAFT_FLIGHT_DYNAMICS']['headerlines_sat_OEM'+str(i+1)])
    #                filename = os.path.join('data', file)
    #                    f = open(filename, "r")
    #                    # Read the contents
    #                    contents = f.readlines()[headerlines:]
    #                    f.close()
    #                    # Split the data based on the comma delimmiter
    #                    data = []
    #                    for line in contents:
    #                        coordinates = line.split() 
    #                        data = data + coordinates
    #                    tot_saved = []
    #                    saved_set = []
    #                    tally = 0
    #                    for value in data:
    #                        if not(value == '\s'):
    #                            val = float(value)
    #                            tally += 1
    #                            if tally < 5:
    #                                # save the data to the set
    #                                saved_set.append(val)
    #                            elif tally == 5:
    #                                # Save the data and the set
    #                                saved_set.append(val)
    #                                tot_saved.append(saved_set)
    #                            else:
    #                                # Reset tally and saved saved set
    #                                saved_set = []
    #                                # Save the data and the set
    #                                saved_set.append(val)
    #                                tally = 1
    #                    satellites_OEM.append(tot_saved)
    #                return stallites_OEM
    
    
    def get_satellite_AEM_data(self):   
        return self.aem_data
    
    def load_satellite_AEM_data(self):
        #        nb_satellites = self.config['SPACECRAFT_FLIGHT_DYNAMICS']['nb_satellites']
        #        if nb_satellite==1:
         # Open the file
        file = self.config['SPACECRAFT_FLIGHT_DYNAMICS']['filename_sat_AEM']
        headerlines =  int(self.config['SPACECRAFT_FLIGHT_DYNAMICS']['headerlines_sat_AEM'])
        #filename = os.path.join('data', file)
        filename = file
        f = open(filename, "r")
        # Read the contents
        contents = f.readlines()[headerlines:]
        f.close()
        # Split the data based on the delimmiter
        data = []
        for line in contents:
            coordinates = line.split() 
            data = data + coordinates
        self.aem_data = []
        saved_set = []
        tally = 0
        for value in data:
            if not(value == '\s'):
                val = float(value)
                tally += 1
                if tally < 6:
                    # save the data to the set
                    saved_set.append(val)
                elif tally == 6:
                    # Save the data and the set
                    saved_set.append(val)
                    self.aem_data.append(saved_set)
                else:
                    # Reset tally and saved saved set
                    saved_set = []
                    # Save the data and the set
                    saved_set.append(val)
                    tally = 1

        # self.aem_data ready
    #        else:
    #            satellites_AEM= []
    #            for i in range(nb_satellites):
    #                # Open the file
    #                file = self.config['SPACECRAFT_FLIGHT_DYNAMICS']['filename_sat_AEM'+str(i+1)]
    #                headerlines =  int(self.config['SPACECRAFT_FLIGHT_DYNAMICS']['headerlines_sat_AEM'+str(i+1)])
    #                filename = os.path.join("data", file)
    #                f = open(filename, "r")
    #                # Read the contents
    #                contents = f.readlines()[headerlines:]
    #                f.close()
    #                # Split the data based on the delimmiter
    #                data = []
    #                for line in contents:
    #                    coordinates = line.split() 
    #                    data = data + coordinates
    #                tot_saved = []
    #                saved_set = []
    #                tally = 0
    #                for value in data:
    #                    if not(value == '\s'):
    #                        val = float(value)
    #                        tally += 1
    #                        if tally < 6:
    #                            # save the data to the set
    #                            saved_set.append(val)
    #                        elif tally == 6:
    #                            # Save the data and the set
    #                            saved_set.append(val)
    #                            tot_saved.append(saved_set)
    #                        else:
    #                            # Reset tally and saved saved set
    #                            saved_set = []
    #                            # Save the data and the set
    #                            saved_set.append(val)
    #                            tally = 1
    #                satellites_AEM.append(tot_saved)
    #            return satellites_AEM
                

    def get_station_OEM_data(self):
        return self.station_oem_data


    def load_station_OEM_data(self):
#        nb_stations = self.config['GROUNDSTATION_LOCATION']['nb_ground_stations']
#        if nb_stations==1:
                    file = self.config['GROUNDSTATION_LOCATION']['filename_GS_OEM']
                    headerlines =  int(self.config['GROUNDSTATION_LOCATION']['headerlines_GS_OEM'])
                    #filename = os.path.join('data', file)
                    filename = file
                    f = open(filename, "r")
                    # Read the contents
                    contents = f.readlines()[headerlines:]
                    f.close()
                    # Split the data based on the comma delimmiter
                    data = []
                    for line in contents:
                        coordinates = line.split() 
                        data = data + coordinates
                    self.station_oem_data = []
                    saved_set = []
                    tally = 0
                    for value in data:
                        if not(value == '\s'):
                            val = float(value)
                            tally += 1
                            if tally < 5:
                                # save the data to the set
                                saved_set.append(val)
                            elif tally == 5:
                                # Save the data and the set
                                saved_set.append(val)
                                self.station_oem_data.append(saved_set)
                            else:
                                # Reset tally and saved saved set
                                saved_set = []
                                # Save the data and the set
                                saved_set.append(val)
                                tally = 1

#        else: 
#            stations_OEM = []
#            for i in range(nb_stations):
#                    file = self.config['GROUNDSTATION_LOCATION']['filename_sta_OEM'+str(i+1)]
#                    headerlines =  int(self.config['GROUNDSTATION_LOCATION']['headerlines_sta_OEM'+str(i+1)])
#                    filename = os.path.join('data', file)
#                    f = open(filename, "r")
#                    # Read the contents
#                    contents = f.readlines()[headerlines:]
#                    f.close()
#                    # Split the data based on the comma delimmiter
#                    data = []
#                    for line in contents:
#                        coordinates = line.split() 
#                        data = data + coordinates
#                    tot_saved = []
#                    saved_set = []
#                    tally = 0
#                    for value in data:
#                        if not(value == '\s'):
#                            val = float(value)
#                            tally += 1
#                            if tally < 5:
#                                # save the data to the set
#                                saved_set.append(val)
#                            elif tally == 5:
#                                # Save the data and the set
#                                saved_set.append(val)
#                                tot_saved.append(saved_set)
#                            else:
#                                # Reset tally and saved saved set
#                                saved_set = []
#                                # Save the data and the set
#                                saved_set.append(val)
#                                tally = 1
#                    stations_OEM.append(tot_saved)
#            return stations_OEM
         
           
    def getTimeStepList(self):
        Sat_OEM = self.get_satellite_OEM_data()
        TimeDay = []
        for line in Sat_OEM:
            TimeDay.append(line[0])    
        TimeSec = []
        for line in Sat_OEM:
            TimeSec.append(line[1])
        TimeScale = [0]*len(TimeSec)
        for i in range(len(TimeSec)):
            TimeScale[i] = TimeDay[i] + (TimeSec[i]/86400) - TimeDay[0]
            i += 1
        return TimeScale
    
    def getSatellitePositionList(self):
        Sat_OEM = self.get_satellite_OEM_data()
        Pos_EME_Sat =[]   
        for line in Sat_OEM:
            Pos_EME_Sat.append(line[-3:])
        return Pos_EME_Sat
        
    def getStationPositionList(self):
        Station_OEM = self.get_station_OEM_data()
        Pos_EME_Sta =[]   
        for line in Station_OEM:
            Pos_EME_Sta.append(line[-3:])
        return Pos_EME_Sta

    def getSatelliteAttitudeQuaternionList(self):
        Sat_AEM = self.get_satellite_AEM_data()
        Q_Sat_2000 = []
        for line in Sat_AEM:
            Q_Sat_2000.append(line[-4:])
        return Q_Sat_2000
    
    def getStationSatelliteVectorList(self):
        Pos_EME_Sat = self.getSatellitePositionList() 
        Pos_EME_Sta = self.getStationPositionList()   
        V_SS_2000 = []
        for i in range(len(Pos_EME_Sat)):
            vect = np.asarray(Pos_EME_Sta[i]) - np.asarray(Pos_EME_Sat[i])
            V_SS_2000.append(vect)
        return V_SS_2000
    
    def getStationSatelliteDistanceList(self):
        V_SS_2000 = self.getStationSatelliteVectorList()
        Mag_Vss_2000 = []
        for vect in V_SS_2000:
            Mag_Vss_2000.append(np.linalg.norm(vect))
        return Mag_Vss_2000
                
    def getAntennaGainTable(self):
        file = self.config['SPACECRAFT_ANTENNA']['filename_ant_gain']
        #filename = os.path.join('data', file)
        filename = file
        f = open(filename, "r")
        has_header = csv.Sniffer().has_header(f.read())
        f.seek(0)
        if has_header:
            next(f) #skip header row
        content = csv.reader(f, delimiter=';', quoting=csv.QUOTE_NONNUMERIC)
        el_az = []
        gain = []
        for row in content: 
            el_az.append([row[0],row[1]])
            gain.append(row[2])
        return el_az,gain


    def compute_downlink_losses(self):
        tx_losses = self.compute_downlink_transmitter_losses()      
        path_loss = self.compute_downlink_path_losses()      
        rx_losses = self.compute_downlink_receiver_losses()
        return tx_losses + path_loss + rx_losses
    
    def compute_downlink_losses_dynamic(self):
        losses = self.compute_downlink_path_losses_dynamic()
        for loss in losses:
            loss += self.compute_downlink_transmitter_losses_dynamic() + self.compute_downlink_receiver_losses_dynamic()
        return losses
    
    def compute_downlink_transmitter_losses(self):
        losses_internalSat = self.compute_downlink_transmitter_losses_dynamic()
        losses_internalSat +=  float(self.config['SPACECRAFT_TRANSMITTER']['SC_loss_point_tx'])   
        return losses_internalSat
    
    def compute_downlink_transmitter_losses_dynamic(self):
        losses_internalSat = float(self.config['SPACECRAFT_TRANSMITTER']['SC_misc'])   
        losses_internalSat += float(self.config['SPACECRAFT_TRANSMITTER']['SC_loss_cable_tx'])   
        losses_internalSat += float(self.config['SPACECRAFT_TRANSMITTER']['SC_loss_connector_tx'])   
        losses_internalSat +=  float(self.config['SPACECRAFT_TRANSMITTER']['SC_loss_feeder_tx'])
        # Removing pointing loss (worst case) as attitude and antenna gain are computed dynamicaly
        return losses_internalSat

    def compute_downlink_receiver_losses(self):
        return self.compute_downlink_receiver_losses_dynamic()
    
    def compute_downlink_receiver_losses_dynamic(self):
        losses_internalGrd = float(self.config['GROUND_STATION_RECEIVER']['GS_loss_point_rx'])   
        #losses_internalGrd += float(self.config['GROUND_STATION_RECEIVER']['GS_loss_cable_rx'])             
        #losses_internalGrd += float(self.config['GROUND_STATION_RECEIVER']['GS_loss_connector_rx'])   
        #losses_internalGrd += float(self.config['GROUND_STATION_RECEIVER']['GS_loss_cable_D_rx'])          
        #same as static for now, implement ground antenna pointing later
        return losses_internalGrd 

    def computeAdditionalAtmLosses(self):  
        additionalAtmLosses = 0
        additionalAtmLosses += float(self.config['PROPAGATION_LOSSES']['loss_pol']) 
        additionalAtmLosses += float(self.config['PROPAGATION_LOSSES']['loss_atm']) 
        additionalAtmLosses += float(self.config['PROPAGATION_LOSSES']['loss_scin']) 
        additionalAtmLosses += float(self.config['PROPAGATION_LOSSES']['loss_rain']) 
        additionalAtmLosses += float(self.config['PROPAGATION_LOSSES']['loss_cloud']) 
        additionalAtmLosses += float(self.config['PROPAGATION_LOSSES']['loss_si']) 
        additionalAtmLosses += float(self.config['PROPAGATION_LOSSES']['loss_misc'])   
        return additionalAtmLosses


    def compute_uplink_transmitter_losses(self):
        losses_internalGS = 0
        losses_internalGS += float(self.config['GROUND_STATION_TRANSMITTER']['GS_line_loss_tx'])   
        losses_internalGS += float(self.config['GROUND_STATION_TRANSMITTER']['GS_loss_connector_tx'])   
        losses_internalGS += float(self.config['GROUND_STATION_TRANSMITTER']['GS_loss_point_rx'])   
        return losses_internalGS
    
    def compute_uplink_transmitter_losses_dynamic(self):
        return self.compute_uplink_transmitter_losses()  #same as static for now, implement ground antenna pointing later
        
    def compute_uplink_receiver_losses(self):
        losses_internalSC = 0
        losses_internalSC += float(self.config['SPACECRAFT_RECEIVER']['SC_loss_point_rx'])   
        #losses_internalSC += float(self.config['SPACECRAFT_RECEIVER']['SC_loss_cable_rx'])             
        #losses_internalSC += float(self.config['SPACECRAFT_RECEIVER']['SC_loss_connector_rx'])         
        return losses_internalSC
    
    def compute_uplink_receiver_losses_dynamic(self):
        losses_internalSC = 0
        # Removing pointing loss (worst case) as attitude and antenna gain are computed dynamicaly
        losses_internalSC += float(self.config['SPACECRAFT_RECEIVER']['SC_loss_cable_rx'])             
        losses_internalSC += float(self.config['SPACECRAFT_RECEIVER']['SC_loss_connector_rx'])         
        return losses_internalSC

    def compute_uplink_losses(self):
        
        
        return self.compute_uplink_transmitter_losses() + self.compute_uplink_path_losses() + self.compute_uplink_receiver_losses()

    def compute_uplink_losses_dynamic(self):
        losses = self.compute_uplink_path_losses_dynamic()
        for loss in losses:
            loss += self.compute_uplink_transmitter_losses_dynamic()  + self.compute_uplink_receiver_losses_dynamic()
        return losses


    def compute_downlink_path_losses(self):
        c = float(self.config['CONSTANTS']['c'])
        f = float(self.config['DOWNLINK']['frequency']) 
        waveLength = c/f        
        return self.computePathLosses(waveLength)
    
    def compute_downlink_path_losses_dynamic(self):
        c = float(self.config['CONSTANTS']['c'])
        f = float(self.config['DOWNLINK']['frequency']) 
        waveLength = c/f        
        return self.computePathLossesDynamic(waveLength)
       
       
    def compute_uplink_path_losses(self):
        c = float(self.config['CONSTANTS']['c'])
        f = float(self.config['UPLINK']['frequency']) 
        waveLength = c/f        
        return self.computePathLosses(waveLength)

    def compute_uplink_path_losses_dynamic(self):
        c = float(self.config['CONSTANTS']['c'])
        f = float(self.config['UPLINK']['frequency']) 
        waveLength = c/f        
        return self.computePathLossesDynamic(waveLength)


    def computePathLosses(self,waveLength):   
        
        additionalAtmLosses = self.computeAdditionalAtmLosses()  
        try:
            elevation = float(self.config['GROUNDSTATION_LOCATION']['GS_minElevation'])
        except KeyError:
            elevation = 10
           
        sr = getSlantRange(self.altitude,elevation)    
        if self.verbose: prettyPrint("SR (km)",sr)        
        fsl = getFreeSpaceLoss(sr*1000,waveLength)
        FSL_dB = to_db(fsl)
        if self.verbose: prettyPrint("FSL_dB",FSL_dB)  
        if self.verbose: prettyPrint("supp Atm Losses (dB)",additionalAtmLosses)         
        lossesSum = FSL_dB +additionalAtmLosses  
        return lossesSum

    
    def computePathLossesDynamic(self,wavelength):
        additionalAtmLosses = self.computeAdditionalAtmLosses()
        lossesSum = []
        Mag_Vss_2000 = self.getStationSatelliteDistanceList()
        for distance in Mag_Vss_2000:
            lossesSum.append(getFreeSpaceLoss_dB(1000*distance,wavelength)+additionalAtmLosses)
        return lossesSum
    
    def computeGroundStationAntennaGain(self):
        try:
            eta_r = float(self.config['GROUND_STATION_RECEIVER']['eta_rx'])
            D=float(self.config['GROUND_STATION_RECEIVER']['antennaDiameter'])
            
            c = float(self.config['CONSTANTS']['c'])
            f = float(self.config['DOWNLINK']['frequency']) 
            waveLength = c/f   
            
            gain_r = eta_r * (math.pi * D / waveLength)**2
            if self.verbose: prettyPrint("gain_r",gain_r)
            gain_r_dB = to_db(gain_r)
            
        except KeyError: #antennaDiameter
            gain_r_dB = float(self.config['GROUND_STATION_RECEIVER']['GS_ant_gain_rx'])
            
        return gain_r_dB
    
    def computeDownlinkNoisePowerSpectralDensity(self):
        k_dB = float(self.config['CONSTANTS']['k_dB']) 
        T_r = float(self.config['GROUND_STATION_RECEIVER']['GS_T_rx']) #Kelvin
        T_r_dB = to_db(T_r)
        return k_dB + T_r_dB
    
    def computeUplinkNoisePowerSpectralDensity(self):
        k_dB = float(self.config['CONSTANTS']['k_dB'])
        try:    
            T_r = float(self.config['SPACECRAFT_RECEIVER']['SC_T_rx']) #Kelvin
            T_r_dB = to_db(T_r)
        except KeyError:
            NF = float(self.config['SPACECRAFT_RECEIVER']['SC_NF_rx'])
            T_r = 280 * ((10**(NF/10))-1)
            T_r_dB = to_db(T_r)
        return k_dB + T_r_dB

    def getGSAntennaGainRX(self):
        try:  
            eta_r = float(self.config['GROUND_STATION_RECEIVER']['eta_rx'])
            D=float(self.config['GROUND_STATION_RECEIVER']['antennaDiameter'])
            
            #self.addToSynthesis("Downlink","Rx","eta_rx",eta_r)
            if self.verbose: prettyPrint("eta_rx",eta_r)
            if self.verbose: prettyPrint("D (m)",D)
            
            c = float(self.config['CONSTANTS']['c'])
            f = float(self.config['DOWNLINK']['frequency']) 
            waveLength = c/f   
            
            gain_r = eta_r * (math.pi * D / waveLength)**2
            
            #if self.verbose: prettyPrint("gain_r",gain_r)
            gain_r_dB = to_db(gain_r)
        except KeyError: #antennaDiameter
            if self.verbose: print("missing param, trying to use directly GS_ant_gain_rx...")
            gain_r_dB = float(self.config['GROUND_STATION_RECEIVER']['GS_ant_gain_rx']) 
        return gain_r_dB
         
    
    
    def computeC_N0_downlink(self):
        """According to config file, return raw link budget margin in dB"""   
        
        epsilon = math.radians(5) #radians  #TODO config 
        R_e = float(self.config['CONSTANTS']['radius_earth'])
        r = self.altitude + R_e
 
        ##################
        if self.verbose: print("------Downlink TRANSMITTER-----")   
        self.synthPart = "Downlink"
        self.synthSubPart = "Tx"        
        P_t = float(self.config['SPACECRAFT_TRANSMITTER']['SC_power_tx']) 
        self.addToSynthesis("SC_power_tx (W)",P_t)          
        P_t_dB = to_db(P_t)
        

        
        self.addToSynthesis("SC_power_tx",P_t_dB)
        
        if self.modeRadioHam: 
            gain_tx_dB = float(self.config['SPACECRAFT_TRANSMITTER']['SC_ant_gain_tx'])
            self.addToSynthesis("gain_tx_dB",gain_tx_dB,"dBi")
            
            #gain_tx_dB = gain_tx_dBi/2.14
            #if self.verbose: print("gain_tx_dB (dB):"+str(gain_tx_dB))              
            #P_t = float(self.config['SPACECFRACT_TRANSMITTER']['SC_power_tx']) 
            #P_t_dB = to_db(P_t)
       
        else:    
            sinTheta = math.cos(epsilon)*R_e/r
            theta = math.asin(sinTheta) 
                
            deltaTheta = math.radians(float(self.config['GROUND_STATION_RECEIVER']['deltaTheta']) )
            theta3db = 2*theta + 2*deltaTheta #in radians !
            self.addToSynthesis("theta3db",theta3db,"degree")
    
            theta3db_degree=math.degrees(theta3db)
            theta_degree=math.degrees(theta)
    
            eta = float(self.config['GROUND_STATION_RECEIVER']['eta_rx'])     
            G_max_dB = eta * (70*math.pi/theta3db_degree)**2
            
        
            G_theta_dB = G_max_dB - 12*(theta_degree/theta3db_degree)**2
            self.addToSynthesis("G_theta_dB",G_theta_dB)
        
            '''
            fromBER_BPSKrequierment = float(self.config['DOWNLINK']['fromBER_BPSKrequierment']) 
            Eb_N0 = fromBER_BPSKrequierment          
            data_rate = float(self.config['DOWNLINK']['data_rate']) 
            c_n0 =  Eb_N0 +  to_db(data_rate)      
            P_t_dB = c_n0 - np.array(G_theta_dB) - gain_r_dB + losses + k_dB + T_r_dB 
            '''
            gain_tx_dB = G_theta_dB
            
            
        self.addToSynthesis("SC_EIRP_TX",self.computeDownlinkEIRP_TX())
        
        tx_losses = self.compute_downlink_transmitter_losses()
        self.addToSynthesis("tx_losses",tx_losses) 
        self.addToSynthesis("EIRP depointing",self.computeDownlinkEIRP_TX()-tx_losses)         
            
        
        if self.verbose: print("------Downlink PATH        -----")
        self.synthSubPart = "Path"
        k_dB = float(self.config['CONSTANTS']['k_dB']) 
        #pylink.utils.from_db(3)           
        self.addToSynthesis("k_dB",k_dB) 
        path_loss = self.compute_downlink_path_losses()
        self.addToSynthesis("path_loss",path_loss) 
        
        
        ###################
        if self.verbose: print("------Downlink RECEIVER      -----")
        self.synthSubPart = "Rx"
        
        T_r = float(self.config['GROUND_STATION_RECEIVER']['GS_T_rx']) #Kelvin
        self.addToSynthesis("T_r (K)",T_r)        
        T_r_dB = to_db(T_r)
        self.addToSynthesis("T_r_dB (dB)",T_r_dB)
            
                  
        ############
        #calculating gain
        gain_antenna_dB = self.getGSAntennaGainRX()
        self.addToSynthesis("gain_r (dBi)",gain_antenna_dB)
        self.addToSynthesis("G/T_rx (dB)",gain_antenna_dB-T_r_dB) 
        print(gain_antenna_dB)   
        
        #LNA amplifier ?
        gain_LNA_dB = float(self.config['GROUND_STATION_RECEIVER']['GS_LNA_gain'])
        self.addToSynthesis("LNA rx (dB)",gain_LNA_dB)          
        gain_rx_dB = gain_antenna_dB #+ gain_LNA_dB
        self.addToSynthesis("total gain (dB)",gain_rx_dB)           
        
        rx_losses = self.compute_downlink_receiver_losses()
        self.addToSynthesis("rx_losses",rx_losses)

        if self.verbose: print("------Downlink SYNTHESIS   -----") 
        self.synthSubPart = "Synthesis"
        
        
        losses = tx_losses + path_loss + rx_losses  
        self.addToSynthesis("TOT losses",losses)        
            
        c_n0 = P_t_dB + gain_tx_dB + gain_rx_dB - losses - k_dB - T_r_dB  
        self.addToSynthesis("C/N0 Downlink",c_n0)   
        return c_n0            


    def computeC_N0_uplink(self):      
        if self.verbose: print("------Uplink Transmitter-----")     
        self.synthPart = "Uplink"
        self.synthSubPart = "Tx"           
        
        gain_tx_dB = float(self.config['GROUND_STATION_TRANSMITTER']['GS_ant_gain_tx'])  
        P_t =  float(self.config['GROUND_STATION_TRANSMITTER']['GS_power_tx']) 
        self.addToSynthesis("P_t (W)",P_t)          
        P_t_dB = to_db(P_t)
        
        self.addToSynthesis("P_t (dB)",P_t_dB)  
        self.addToSynthesis("gain_tx_dB (dB)",gain_tx_dB)          
        
        
        self.addToSynthesis("GS_EIRP_TX ",self.computeUplinkEIRP_TX())  
        
        tx_losses = self.compute_uplink_transmitter_losses()
        self.addToSynthesis("tx_losses",tx_losses)     
        self.addToSynthesis("EIRP depointing ",self.computeUplinkEIRP_TX()-tx_losses)         
  
        if self.verbose: print("------Uplink PATH       -----")
        self.synthSubPart = "Path" 
        k_dB = float(self.config['CONSTANTS']['k_dB']) 
        #pylink.utils.from_db(3)           
        self.addToSynthesis("k_dB (dB)",k_dB)
        
        path_loss = self.compute_downlink_path_losses()
        self.addToSynthesis("path_loss",path_loss)         

        
        if self.verbose: print("------Uplink RECEIVER   -----")    
        self.synthSubPart = "Rx"
        T_r = float(self.config['SPACECRAFT_RECEIVER']['SC_T_rx']) #Kelvin
        self.addToSynthesis("T_r (K)",T_r)        
        T_r_dB = to_db(T_r)
        self.addToSynthesis("T_r_dB (dB)",T_r_dB)           
        
        rx_losses = self.compute_uplink_receiver_losses()
        self.addToSynthesis("rx_losses",rx_losses)

        gain_antenna_dB = float(self.config['SPACECRAFT_RECEIVER']['SC_ant_gain_rx'])
        self.addToSynthesis("Antenna gain (dBi)",gain_antenna_dB) 
        
        #LNA amplifier ?
        gain_LNA_dB = float(self.config['SPACECRAFT_RECEIVER']['SC_LNA_gain'])
        self.addToSynthesis("LNA rx (dB)",gain_LNA_dB)          
        gain_rx_dB = gain_antenna_dB #+ gain_LNA_dB
        self.addToSynthesis("total gain (dB)",gain_rx_dB)        
        
        if self.verbose: print("------Uplink SYNTHESIS   -----") 
        self.synthSubPart = "Synthesis"       
        losses = tx_losses + path_loss + rx_losses
        self.addToSynthesis("TOT losses",losses)
        c_n0 = P_t_dB + gain_tx_dB + gain_rx_dB - losses - k_dB - T_r_dB
        self.addToSynthesis("C/N0 Uplink",c_n0)          
        return c_n0


    def computeDownlinkEIRP_TX(self):
       """ EIRP: Effective (or equivalent) Isotropic Radiated Power
       PIRE en fr - Puissance Isotrope Rayonnée Equivalente
       en dB
       """
       P_tx =  float(self.config['SPACECRAFT_TRANSMITTER']['SC_power_tx'])  
       P_tx_dB = to_db(P_tx)
       gain_tx_dB = float(self.config['SPACECRAFT_TRANSMITTER']['SC_ant_gain_tx'])  
       EIRP = P_tx_dB + gain_tx_dB
       return EIRP
   
    
    def computeUplinkEIRP_TX(self):
       """ EIRP: Effective (or equivalent) Isotropic Radiated Power
       PIRE en fr - Puissance Isotrope Rayonnée Equivalente
       en dB
       """
       P_tx =  float(self.config['GROUND_STATION_TRANSMITTER']['GS_power_tx']) 
       P_tx_dB = to_db(P_tx)
       gain_tx_dB = float(self.config['GROUND_STATION_TRANSMITTER']['GS_ant_gain_tx'])  
       EIRP = P_tx_dB + gain_tx_dB
       return EIRP    
   

    def computeEb_N0_Downlink_manual(self,data_rate,margin=0):
            """According to config file, return Eb_N0 depending on data 
            rate selected (in bits per second), and optionally with a margin"""  
            
            try:
                c_n0 = float(self.synthesis["Downlink"]["Synthesis"]["C/N0 Downlink"][0])
            except:
                print("NotFound:c_n0 calculating!")
                c_n0 = self.computeC_N0_downlink()
            Eb_N0 = c_n0 - to_db(data_rate)-  margin    
            
            return Eb_N0
    
    def computeEb_N0_Downlink(self):
            """According to config file, return Eb_N0 depending on data 
            rate selected (in bits per second), and optionally with a margin"""  
            try:
                c_n0 = float(self.synthesis["Downlink"]["Synthesis"]["C/N0 Downlink"][0])
            except:
                print("NotFound:c_n0 calculating!")
                c_n0 = self.computeC_N0_downlink()
            
            margin = float(self.config['DOWNLINK']['system_margin']) 
            self.addToSynthesis("system_margin (dB)",margin)         
            data_rate = float(self.config['DOWNLINK']['data_rate']) 
            self.addToSynthesis("data_rate (bps)",data_rate)
            self.addToSynthesis("data_rate (dB)",to_db(data_rate))            
            #self.addToSynthesis("C/N0 downlink",c_n0)
            
            Eb_N0 = c_n0 - to_db(data_rate)-  margin 
            self.addToSynthesis("Eb_N0 downlink",Eb_N0)
            return Eb_N0
        
    def computeEb_N0_Uplink_manual(self,data_rate,margin=0):
            """According to config file, return Eb_N0 depending on data 
            rate selected (in bits per second), and optionally with a margin"""  
            try:
                c_n0 = float(self.synthesis["Uplink"]["Synthesis"]["C/N0 Uplink"][0])
            except:
                print("NotFound:c_n0 calculating!")
                c_n0 = self.computeC_N0_uplink()
            #if self.verbose: prettyPrint("c_n0",c_n0)       
            Eb_N0 = c_n0 - to_db(data_rate)-  margin            
            return Eb_N0

        
    def computeEb_N0_Uplink(self):
            """According to config file, return Eb_N0 depending on data rate 
            selected (in bits per second)"""  
            margin = float(self.config['UPLINK']['system_margin']) 
            if self.verbose: prettyPrint("system_margin (dB)",margin)            
            data_rate = float(self.config['UPLINK']['data_rate']) 
            
            self.addToSynthesis("data_rate (bps)",data_rate)
            self.addToSynthesis("data_rate (dB)",to_db(data_rate))              
              
            try:
                c_n0 = float(self.synthesis["Uplink"]["Synthesis"]["C/N0 Uplink"][0])
            except:
                print("NotFound:c_n0 calculating!")
                c_n0 = self.computeC_N0_uplink()  
            
            Eb_N0 = c_n0 - to_db(data_rate)-  margin       
            self.addToSynthesis("Eb_N0 uplink",Eb_N0)            
            return Eb_N0
 
    def computeSystemMarginDownlink(self):
        required_BER_from_Modulation  = float(self.config['DOWNLINK']['required_BER_from_Modulation']) 
        self.addToSynthesis("Eb/N0 Threshold",required_BER_from_Modulation)
        
        try:
            eb_N0 = float(self.synthesis["Downlink"]["Synthesis"]["Eb_N0 downlink"][0])
        except:
            print("NotFound: Eb_N0_Downlink calculating!")            
            eb_N0 = self.computeEb_N0_Downlink()
        
        margin =  eb_N0 - required_BER_from_Modulation
        self.addToSynthesis("Downlink margin",margin)  
        return margin


    def computeSystemMarginUplink(self):
        required_BER_from_Modulation = float(self.config['UPLINK']['required_BER_from_Modulation']) 
        if self.verbose: prettyPrint("Eb/N0 Threshold",required_BER_from_Modulation)
        
        try:
            eb_N0 = float(self.synthesis["Downlink"]["Synthesis"]["Eb_N0 uplink"][0])
        except:
            eb_N0 = self.computeEb_N0_Uplink()
        
        margin =  eb_N0 - required_BER_from_Modulation        
        self.addToSynthesis("Uplink margin",margin)         
        return margin
        
    
'''
    linkMargindB
'''


def getPowerTX(configFilename,altitude,verbose=False):     
    """
    @param: 
    According to config file, return required Power for satisfying link 
    budget, depending on orbit altitude"""   
    if configFilename[-5:]==".json":
        config = json.load(open(configFilename))  
             
    else:
        config = configparser.ConfigParser()
        config.read(configFilename)     
    h = altitude

    epsilon = math.radians(5) #radians  
    
    R_e = float(config['CONSTANTS']['radius_earth'])
    r = np.array(h) + R_e
    
        
    ##################
    if verbose: print("TRANSMITTER")
    sinTheta = math.cos(epsilon)*R_e/r
    theta = [math.asin(x) for x in sinTheta]
    if verbose: print("Theta:"+str(list(map(math.degrees,theta))))

    deltaTheta = math.radians(float(config['GROUND_STATION_RECEIVER']['deltaTheta']) )
    theta3db = 2*np.array(theta) + 2*deltaTheta #in radians !
    if verbose: print("theta3db:"+str(list(map(math.degrees,theta3db))))
       
    sl = [getSlantRange(altitude) for altitude in h]
    if verbose: print("sl"+str(sl))
     
    c = float(config['CONSTANTS']['c'])
    f = float(config['DOWNLINK']['frequency']) 
    waveLength = c/f
    
    FSL = [getFreeSpaceLoss(r*1000,waveLength) for r in sl]
    FSL_dB = [to_db(fsl) for fsl in FSL]
    
    if verbose: print("FSL"+str(FSL_dB))
        
        
        
    theta3db_degree=np.array(list(map(math.degrees,theta3db)))
    theta_degree=np.array(list(map(math.degrees,theta)))
    
    
    eta = float(config['GROUND_STATION_RECEIVER']['eta'])     
    G_max_dB = eta * (70*math.pi/theta3db_degree)**2
    if verbose: print("G_max_dB"+str(G_max_dB))
    

    G_theta_dB = np.zeros(4) 
    for i in range(0,len(G_max_dB)):
        G_theta_dB[i] = G_max_dB[i] - 12*(theta_degree[i]/theta3db_degree[i])**2
    if verbose: print("G_theta_dB"+str(G_theta_dB))
    
    
    ###################
    if verbose: print("RECEIVER")
    eta_r = float(config['GROUND_STATION_RECEIVER']['eta_rx'])
    D=float(config['GROUND_STATION_RECEIVER']['antennaDiameter'])
    gain_r = eta_r * (math.pi * D / waveLength)**2
    gain_r_dB = to_db(gain_r)
    
    
    
    if verbose: print("gain_r:"+str(gain_r))
    if verbose: print("gain_r in dB:"+str(gain_r_dB))
    
    
    T_r = float(config['GROUND_STATION_RECEIVER']['GS_T_rx']) #Kelvin
    T_r_dB = to_db(T_r)
    
    k_dB = float(config['CONSTANTS']['k_dB']) 
    #pylink.utils.from_db(3)
    
    
    ############
    if verbose: print("Linking both")
    #requirement for BSPK and  BER  10-6 +margin
    #TODO funtion to auto get margin
    margin = float(config['DOWNLINK']['system_margin']) 
    fromBER_BPSKrequierment = float(config['DOWNLINK']['required_BER_from_Modulation']) 
    
    #Eb_N0 = fromBER_BPSKrequierment +margin
    Eb_N0 = fromBER_BPSKrequierment
    #Eb_N0 = 3
    if verbose: print("Eb_N0 "+str(Eb_N0))    
    
    data_rate = float(config['DOWNLINK']['data_rate']) 
    c_n0 =  Eb_N0 +  to_db(data_rate)
    if verbose: print("c_n0 "+str(c_n0))  
    
    if verbose: print("k_dB:"+str(k_dB))
    if verbose: print("T_r_dB:"+str(T_r_dB))     
    
    
    losses_atm_rain = float(config['PROPAGATION_LOSSES']['loss_atm']) 
    losses_misc = float(config['PROPAGATION_LOSSES']['loss_misc']) 
    losses_internalSat = float(config['SPACECRAFT_TRANSMITTER']['SC_misc']) 


    losses = np.array(FSL_dB) +losses_misc + losses_atm_rain + losses_internalSat   
    if verbose: print("losses:"+str(losses))
    

    P_t_dB = c_n0 - np.array(G_theta_dB) - gain_r_dB + losses + k_dB + T_r_dB 

   
    
    if verbose: print("P_t_dB"+str(P_t_dB))
    P_t_dB_margin = P_t_dB + margin
    if verbose: print("P_t_dB_margin"+str(P_t_dB_margin))    
    P_t_margin = [from_db(p) for p in P_t_dB_margin]
    if verbose: print("P_t (W)"+str(P_t_margin))
    
    #EIRP_satellite_req_dB = c_n0 - gain_r_dB + np.array(FSL_dB)
    #print("EIRP_satellite_req_dB"+str(EIRP_satellite_req_dB))
    
    P_t_real = np.array(P_t_margin)/0.2
    if verbose: print("P_t_real (W)"+str(P_t_real))
    return P_t_real
   
"""    
configFile = "Data/luplink_Creme_S.ini"
lBA = LinkBudgetAnalysis(configFile,True) 
self = lBA
verbose=True
altitude=None
"""

def getLinkBugdet(configFile,altitude=None,verbose=False):
    """ return a report corresponding to link budget calculus
    for uplink and downlink of config file"""
    
    report = {}

    print ("Working on <"+str(configFile)+">")
    #stream = open(configFile, 'r')
    #config = yaml.safe_load(stream)

    stream = open(configFile, 'r')
    config = yaml.load(stream, Loader = yamlloader.ordereddict.CSafeLoader)
    config = fdelUnits(config)
    lBA = LinkBudgetAnalysis(config,verbose)
    
    
    if not altitude == None:
        lBA.altitude = altitude
        print ("Warning: manually changing altitude. Value is:  <"+str(altitude)+" [km]>")
    
    
    #print("================")    
    #prettyPrint("Downlink Transmitter Losses",lBA.compute_downlink_transmitter_losses())
    #64.47940008672037
    #P_t 3.712306827960641
    #prettyPrint("Downlink GS RX Gain",lBA.getGSAntennaGainRX())

    if lBA.verbose : print("================ DOWNLINK ===============")    
    lBA.computeC_N0_downlink()
    #print(lBA.synthesis["Downlink"]["Synthesis"]['C/N0 Downlink'][0]) 
    lBA.computeEb_N0_Downlink()
    
    
    
    #print("================")
    data_rate = 250000 # (bit/s)
    #data_rate = 100000 # (bit/s)    
    margin = 0 #(dB)
    if lBA.verbose :prettyPrint("EB/N0 if 250kbps",lBA.computeEb_N0_Downlink_manual(data_rate,margin)) 
    
    
    lBA.computeSystemMarginDownlink()   

    
    if lBA.verbose : print("================ UPLINK =================")
    lBA.computeC_N0_uplink()
    
    #print(lBA.synthesis["Uplink"]["Synthesis"]['C/N0 Uplink'][0])     
    #lBA.computeEb_N0_Uplink()
    #print("================")    
    data_rate = 250000 # (bit/s)
    #data_rate = 100000 # (bit/s)    
    margin = 0 #(dB)
    if lBA.verbose : prettyPrint("EB/N0 if 250kbps",lBA.computeEb_N0_Uplink_manual(data_rate,margin))  
    lBA.computeSystemMarginUplink()      

    report = lBA.synthesis
    
    return report
    

def testReport(lBA):
    print("-------------------------------------------------------- ")
    #prettyD(lBA.synthesis)
    print("-------------------------------------------------------- ")
    r1 = lBA.synthesis["Downlink"]["Synthesis"]
    prettyD(r1)
    

    print("-------------------------------------------------------- ")    
    r2=r1["TOT losses"] 
    print(str(r2))
    r2=r1['C/N0 Downlink'] 
    print(str(r2))   
    print(lBA.synthesis["Downlink"]["Synthesis"]['C/N0 Downlink'] )  




def prettyC(myClass):
    attrs = vars(myClass)
    
    for item in attrs.items():
        #print (item)
        if '__main__' in str(type(item[1])):
            prettyC(item[1])
        else:            
            print(str(item))
            
#prettyC(myClass)   
            
def testClass(report):
    class Synthesis:
        pass
        
    class LinkBudget:
         pass 
            
    class Downlink:
         pass         
    
    mySynthesis = Synthesis()
    mySynthesis.linkBudget = LinkBudget()
    mySynthesis.linkBudget.dowlink = Downlink()
    
    mySynthesis.linkBudget.dowlink.C_N0 = float(report["Downlink"]["Synthesis"]['C/N0 Downlink'][0])
    mySynthesis.linkBudget.dowlink.C_N0_unit = report["Downlink"]["Synthesis"]['C/N0 Downlink'][1]
    
    mySynthesis.linkBudget.dowlink.Eb_N0 = float(report["Downlink"]["Synthesis"]['Eb_N0 downlink'][0])
    mySynthesis.linkBudget.dowlink.Eb_N0_unit = report["Downlink"]["Synthesis"]['Eb_N0 downlink'][1]    
    
    prettyC(mySynthesis)


if __name__ == '__main__':
    parser = OptionParser()
    defaultFile = "data/luplinkDefault"
    #defaultFile =  "data/nimphTest.ini"
    #defaultFile = "Data/luplink_Creme_S"    
    defaultFile = "Data/input.yaml"
    #defaultInitFile = "data/nimphTest.ini"
    #os.system("python2 ini2json.py data/nimphTest.ini > data/nimphTest.json") 
    '''
    res = os.system("python2")
    if res == 0:
        os.system("python2 ini2json.py "+defaultFile+".ini > "+defaultFile+".json")    
    else:
        print("WARNING python2 not found, .ini modification may not have been taken into account")
    
    defaultInitFile = defaultFile+".json"    
    '''    
    defaultInitAlt = "[800,1000,1200,1400]"
    
    parser.add_option("-f", "--file", dest="filename",
                      help="write report to FILE", metavar="FILE",default=defaultFile)
    parser.add_option("-v", "--verbose",
                      action="store_true", dest="verbose", default=False,
                      help="print status messages to stdout")  
    parser.add_option("-a", "--altitude",
                      action="store_true", dest="altitude", default=None,
                      help="provide this altitude instead of .ini altitude parameter")      
    
    
    (options, args) = parser.parse_args()
    
    
    #altitudeTested =  [800,1000,1200,1400]
    #print (getPowerTX(options.filename,altitudeTested,verbose=options.verbose))
    
    if str(options.altitude) == 'None':
        print("Altitude defined in <"+str(options.filename)+">")
    else:
        print("Altitude is manually defined...")
    
    #alti = 700
    #report = getLinkBugdet(options.filename,alti,options.verbose)
    '''
    report = getLinkBugdet(options.filename,options.altitude,options.verbose)
    prettyStr(report)
    
    
    #print(prettyStr(report))
    
    print("-------------------------------------------------------- ")
    
    if options.verbose:
        prettyD(report)
    else:
        print("================ DOWNLINK ===============") 
        prettyD(report["Downlink"]["Synthesis"])
        print("================ UPLINK =================")
        prettyD(report["Uplink"]["Synthesis"])
    '''
    
    
    print("\nSelecting altitude of 700km:")
    report = getLinkBugdet(options.filename,700,True)
    print("================ REPORT MD ===============")     
    stringSyntheseLinkBudget = '|Category|Data|Value|Unit|\n'
    stringSyntheseLinkBudget += '| :---: | :---: | :---: | :---: |\n'
    stringSyntheseLinkBudget += prettyMD(report)
    print (stringSyntheseLinkBudget)
    
    
    
    '''
    print("================ example of different way of handling params instead of using a dictionnary =================")
    testClass(report)
    '''