# -*- coding: utf-8 -*-
"""
@author: tgateau
"""




import unittest
from luplink import getSlantRange,getPowerTX,getFreeSpaceLoss_dB
from luplink import LinkBudgetAnalysis
from luplink import getLinkBugdet

radius_earth = 6378.136 

dataPath = "dataTest/"

class TestLuplink(unittest.TestCase):  
    def setUp(self):
        self.lBA = LinkBudgetAnalysis(dataPath+"nimphTest.ini") 
        
        
    def assertListAlmostEqual(self, list1, list2, tol=7):
        self.assertEqual(len(list1), len(list2))
        for a, b in zip(list1, list2):
             self.assertAlmostEqual(a, b, tol)   
    
    def test_getSlantRange01(self):      
        self.assertAlmostEqual(getSlantRange(800,5,radius_earth), 2783.8741712832734, places=7, msg=None, delta=None)

    def test_getSlantRange02(self):
        self.assertAlmostEqual(getSlantRange(800,90,radius_earth), 800.0, places=7, msg=None, delta=None)

    def test_getSlantRange03(self):      
        self.assertAlmostEqual(getSlantRange(0,0,radius_earth), 0.0, places=7, msg=None, delta=None)

    def test_getPowerTX(self):   
        altitude =  [800,1000,1200,1400]
        powerExpected= [ 18.56153414,  23.54814539,  28.41581038,  33.13792671]
        self.assertListAlmostEqual(list(getPowerTX(dataPath+"nimphTest.ini",altitude,verbose=False)),powerExpected)
        
        
        #153.4727366921119
        
        
    def test_compute_SC_TX_losses(self):  
        self.assertAlmostEqual(self.lBA.compute_downlink_losses(),170.461292398652)
        
        
    def testingDistantDataMerge(self):
        distantData = {'GS_altitude': None, 'SC_altitude': '11', 'GS_minElevation': None}
        
        
        
    def test_getFreeSpaceLossdB(self):
        self.assertAlmostEqual(getFreeSpaceLoss_dB(2045343.667,0.68525714),151.4982544)
        
    def test_getTimeStepList(self):
        self.lBA.loadOEM_AEM()
        self.assertAlmostEqual(self.lBA.getTimeStepList()[5],5.7870369346346706e-05)
        
        
        
    def test_getSatellitePositionList(self):
        self.lBA.loadOEM_AEM()
        self.assertAlmostEqual(self.lBA.getSatellitePositionList()[1][0],-6770.653)
    
    def test_getStationPositionList(self):
        self.lBA.loadOEM_AEM()
        self.assertAlmostEqual(self.lBA.getStationPositionList()[4][1],4544.11272)

        
    def test_getSatelliteAttitudeQuaternionList(self):
        self.lBA.loadOEM_AEM()
        self.assertAlmostEqual(self.lBA.getSatelliteAttitudeQuaternionList()[5][2],0.46416)
        
        
    def test_getStationSatelliteVectorList(self):
        self.lBA.loadOEM_AEM()       
        
        self.assertAlmostEqual(self.lBA.getStationSatelliteVectorList()[21][1],5730.448769999999)
        
    def test_getStationSatelliteDistanceList(self):  
        self.lBA.loadOEM_AEM()
        self.assertAlmostEqual(self.lBA.getStationSatelliteDistanceList()[29],9158.461252543504)
        
    def test_getAntennaGainTable(self):
        self.lBA.loadOEM_AEM()
        el_az,gain = self.lBA.getAntennaGainTable()
        self.assertAlmostEqual(el_az[21][0],-158.357)
        self.assertAlmostEqual(gain[44],-22.41295804)
    
    
    def test_computeCremeIni_C_N0(self):
        report = getLinkBugdet(dataPath+"nimphTest.ini",None,False)
        self.assertEqual(report,{})
        
        
    '''    
    def test_computeC_N0(self):
        self.assertAlmostEqual(self.lBA.computeC_N0(),69.47940008672037)  
        
    def test_computeEb_N0_manual(self):
        self.assertAlmostEqual(self.lBA.computeEb_N0_manual(250000,5),10.5,7)
  
    def test_computeEb_N0(self):
        self.assertAlmostEqual(self.lBA.computeEb_N0(),10.5,7)
        '''              

if __name__ == '__main__':
    unittest.main()
    
